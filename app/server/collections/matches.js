Matches.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

Matches.deny({
  insert: function (userId, doc) {
    if (doc.tags.debug !== 1) return true;

    var mod = Mods.findOne(doc.modId, {fields: {userId: 1}});
    if (mod.userId !== userId) return true;
    
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  },

  fetch: ['tags', 'modId']
});