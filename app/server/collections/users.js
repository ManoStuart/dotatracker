Meteor.users.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

Meteor.users.deny({
  insert: function (userId, doc) {
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    if (_.contains(fieldNames, '_id')) return true;

    if (doc._id !== userId) return true;

    return false;
  },

  remove: function (userId, doc) {
    return true;
  },

  fetch: ['_id', 'userId']
});