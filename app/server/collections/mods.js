Mods.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

Mods.deny({
  insert: function (userId, doc) {
    if (doc.userId !== userId) return true;
    
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    if (_.intersection(fieldNames, ['_id', 'userId', 'key', 'matchCount']).length > 0) return true;

    if (doc.userId !== userId) return true;

    return false;
  },

  remove: function (userId, doc) {
    return true;
  },

  fetch: ['_id', 'userId']
});