Meteor.methods({
  hasGraphs: function (id) {
    if (!this.userId) return false;

    return (Mods.find({userId: this.userId, graphs: {$exists: true, $not: {$size: 0}}}).count() > 0)
  },
  hasMatches: function (id) {
    if (!this.userId) return false;

    var mods = Mods.find({userId: this.userId}, {fields: {_id: 1}}).fetch();
    var modsIds = _.pluck(mods, "_id");

    return (Matches.find({modId: {$in: modsIds}}).count() > 0);
  },
  clearMatches: function (id) {
    var mod = Mods.findOne(id);
    if (!mod) throw new Meteor.Error(404, "Mod not found.");
    if (mod.userId !== this.userId) throw new Meteor.Error(403, 'Not your mod fag.');

    Matches.remove({modId: id});
    Mods.update({_id: id}, {$set: {matchCount: 0}});
  },
  // fuckShit: function () {
  //   Matches.remove({});
  //   Mods.remove({});
  // },
  sendMail: function (subject, content, contact) {
    var data = {
      from: ((contact !== '') ? contact : "modtracker@no-reply.com"),
      to: 'dotatracker.contact@gmail.com',
      subject: "[dotaTracker] [site] " + subject,
      text: content,
    }

    Email.send(data)
  },
});

var graphFunc = {
  count: countPlot,
  live: livePlot,
  related: relPlot,
};