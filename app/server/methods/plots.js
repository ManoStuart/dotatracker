Meteor.methods({
  getPlots: function (query, modifiers) {
    if (!query || !query.modId) throw new Meteor.Error(400, "No mod selected.");
    var mod = Mods.findOne(query.modId);

    if (!mod) throw new Meteor.Error(404, "Mod not found");
    if (mod.userId !== this.userId && !mod.public) throw new Meteor.Error(403, "This Mod is not public.");

    if (!modifiers) modifiers = {limit: 200, sort: {date: -1}};

    var data = Matches.find(query, modifiers).fetch();
    var plot = [];

    mod.graphs.forEach(function (value, index) {
      if (value.func === 'live') return;

      plot.push({
        plot: graphFunc[value.func](data, value),
        id: value.id,
      });
    });

    return plot;
  },
  getPlot: function (id, query, modifiers) {
    if (!query || !query.modId) throw new Meteor.Error(400, "No mod selected.");
    var mod = Mods.findOne(query.modId);

    if (!mod) throw new Meteor.Error(404, "Mod not found");
    if (mod.userId !== this.userId && !mod.public) throw new Meteor.Error(403, "This Mod is not public.");

    if (!modifiers) modifiers = {limit: 200, sort: {date: -1}};

    var data = Matches.find(query, modifiers).fetch();

    var plot = _.findWhere(mod.graphs, {id: id});
    if (!plot) throw new Meteor.Error(400, 'No graph selected.');

    return {
      plot: graphFunc[plot.func](data, plot),
      id: plot.id,
    };
  },
});

var graphFunc = {
  count: countPlot,
  live: livePlot,
  related: relPlot,
};