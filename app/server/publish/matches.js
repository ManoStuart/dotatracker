Meteor.publish('modMatches', function (query, modifiers) {
  if (!query || !query.modId) return;

  // check permissions
  var mod = Mods.findOne(query.modId);
  if (!mod || (mod.userId !== this.userId && !mod.public)) return;

  if (!modifiers) modifiers = {limit: 10, sort: {date: -1}};

  return Matches.find(query, modifiers);
});
