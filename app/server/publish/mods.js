Meteor.publish('myMods', function () {
  return Mods.find({userId: this.userId}, {fields: {key: 0, graphs: 0, public: 0}});
});

Meteor.publish('singleMod', function (slug) {
  if (!slug) return [];

  return Mods.find({
    slug: slug,
    $or: [
      {userId: this.userId},
      {public: true}
    ]
  }, {
    fields: {
      key: 0,
    }
  });
});

Meteor.publish('editMod', function (id) {
  if (!id) return [];

  return Mods.find({
    _id: id,
    userId: this.userId,
  }, {
    fields: {
      graphs: 0,
      public: 0,
      matchCount: 0,
    }
  });
});

Meteor.publish('publicMods', function (limit) {
  if (!limit) limit = 15;

  return Mods.find({
    public: true,
  }, {
    fields: {
      key: 0,
      userId: 0,
      graphs: 0,
    },
    limit: limit,
    sort: {matchCount: -1},
  })
});