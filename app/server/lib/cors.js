var connectHandler = WebApp.connectHandlers; // get meteor-core's connect-implementation

WebApp.connectHandlers.use(function(req, res, next) {
  // add allow origin
  res.setHeader('Access-Control-Allow-Origin', '*');
  
  // add headers
  res.setHeader('Access-Control-Allow-Headers', [
      'Accept',
      'Accept-Charset',
      'Accept-Encoding',
      'Accept-Language',
      'Accept-Datetime',
      'Authorization',
      'Cache-Control',
      'Connection',
      'Cookie',
      'Content-Length',
      'Content-Type',
      'Date',
      'User-Agent',
      'X-Requested-With',
      'Origin'
  ].join(', '));

  return next();
});