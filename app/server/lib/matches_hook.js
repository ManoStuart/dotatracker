Matches.before.insert(function (userId, doc) {
  if (doc.tags.debug !== 1)
    Mods.update({_id: doc.modId}, {$inc: {matchCount: 1}});
});