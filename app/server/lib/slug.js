Mods.before.insert(function (userId, doc) {
  doc.slug = GetSlug(doc);
});

Mods.before.update(function (userId, doc, fieldNames, modifier, options) {
  if (_.contains(fieldNames, 'name') && modifier.$set && modifier.$set.name) {
    doc.name = modifier.$set.name;
    modifier.$set.slug = GetSlug(doc);
  }
});

function GetSlug (doc) {
  var slug = convertToSlug(doc.name);
  var aux = Mods.findOne({slug: slug, _id: {$ne: doc._id}});

  while (aux) {
    slug += '0';
    aux = Mods.findOne({slug: slug, _id: {$ne: doc._id}});
  }

  return slug;
}

var convertToSlug = function (Text) {
  return removerAcentos(Text
      .toLowerCase()
      .replace(/[^\w ]+/g,'')
      .replace(/ +/g,'-'));
}

var removerAcentos = function ( newStringComAcento ) {
  var string = newStringComAcento;
  var mapaAcentosHex  = {
    a : /[\xE0-\xE6]/g,
    e : /[\xE8-\xEB]/g,
    i : /[\xEC-\xEF]/g,
    o : /[\xF2-\xF6]/g,
    u : /[\xF9-\xFC]/g,
    c : /\xE7/g,
    n : /\xF1/g
  };
 
  for ( var letra in mapaAcentosHex ) {
    var expressaoRegular = mapaAcentosHex[letra];
    string = string.replace( expressaoRegular, letra );
  }
 
  return string;
}