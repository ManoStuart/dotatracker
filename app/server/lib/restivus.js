// Global API configuration
Restivus.configure({
  // useAuth: true,
  prettyJson: true
});
  
Meteor.startup(function () {
  // Restivus.addRoute('match', {authRequired: true}, {
  Restivus.addRoute('match/:modId', {}, {
    post: {
      action: function () {
        var params = this.bodyParams;
        // var params = this.urlParams;

        // Validation and security
        var mod = Mods.findOne(this.urlParams.modId);
        if (!mod) return {
          statusCode: 404,
          body: {status: 'fail', message: 'Mod not found.'},
        }
        // if (mod.userId !== this.userId) return {
        //   statusCode: 403,
        //   body: {status: 'fail', message: 'Not your mod fag.'},
        // }
        if (mod.key !== params.key) return {
          statusCode: 403,
          body: {status: 'fail', message: 'Not your mod fag.'},
        }

        var data = {
          modId: this.urlParams.modId,
          tags: {},
          stats: {},
        }

        // Body params parsing
        var tags;
        try {
          data.tags = JSON.parse(params.tags);
          data.stats = JSON.parse(params.stats);
        } catch (e) {
          return {
            statusCode: 500,
            body: {status: 'fail', message: 'Failed to parse your data'},
          }
        }

        // URL params parsing
        // var wrong = [];

        // for (var key in this.urlParams.query) {
        //   if (key.startsWith('tags.')) {
        //     data.tags[key.substring(5)] = getValue(this.urlParams.query[key]);
        //   }
          
        //   else if (key.startsWith('stats.'))
        //     data.stats[key.substring(6)] = getValue(this.urlParams.query[key]);

        //   else
        //     wrong.push(key);
        // }

        // Size check
        var size = roughSizeOfObject(data);
        if (size > 2000) {
          return {
            statusCode: 400,
            message: "Object size must be less than 2kb",
          }
        }

        Matches.insert(data);

        return {status: "success"};
      }
    },
  });
});

function roughSizeOfObject( object ) {
  var objectList = [];
  var stack = [ object ];
  var bytes = 0;

  while ( stack.length ) {
    var value = stack.pop();

    if ( typeof value === 'boolean' ) {
      bytes += 4;
    }
    else if ( typeof value === 'string' ) {
      bytes += value.length * 2;
    }
    else if ( typeof value === 'number' ) {
      bytes += 8;
    }
    else if
    (
      typeof value === 'object'
      && objectList.indexOf( value ) === -1
    )
    {
      objectList.push( value );

      for( var i in value ) {
        stack.push( value[ i ] );
      }
    }
  }
  return bytes;
}