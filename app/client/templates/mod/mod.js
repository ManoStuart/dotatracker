Template.Mod.events({
  'click #editMod': function (e,t) {
    Session.set('modId', t.data._id);
    Router.go('NewMod');
  },
  'click #modPublic': function (e,t) {
    Mods.update({_id: t.data._id}, {$set: {public: (t.data.public) ? false : true}});
  },
  'click #clearMatches': function (e,t) {
    OpenDeleteModal(e.currentTarget.dataset.id, 'clearMatches');
  },
  'click .clearMatches': function (e,t) {
    Meteor.call('clearMatches', t.data._id);
  },
  'click .remove': function (e,t) {
    OpenDeleteModal(e.currentTarget.dataset.id, 'remove-modal');
  },
  'click .remove-modal': function (e,t) {
    var id = e.currentTarget.dataset.id;
    var graph = _.findWhere(t.data.graphs, {id: id});

    // Update user prefered graphs
    Mods.update({_id: t.data._id}, {$pull: {graphs: graph}});
  },
  'click .collapse': function (e,t) {
    $(e.currentTarget).toggleClass('on');
    $(e.currentTarget).parents('.portlet').find('.portlet-body').slideToggle(200);
  },
  'click .reload': function (e,t) {
    ReloadGraph(e.currentTarget.dataset.id);
  },
  'click .config': function (e,t) {
    var id = e.currentTarget.dataset.id;
    Session.set('graphEdit', id);

    var graph = _.findWhere(t.data.graphs, {id: id});

    Session.set('graphXAxis', graph.xaxis.slice());
    Session.set('graphYAxis', graph.yaxis.slice());
    Session.set('curFunc', graph.func);
    resetGraphModal();

    $('#graph-config').modal('show');
  },
  'click #newGraph': function (e,t) {
    Session.set('graphEdit', 'new');
    Session.set('graphXAxis', []);
    Session.set('graphYAxis', []);
    Session.set('curFunc', 'count');
    resetGraphModal();

    $('#graph-config').modal('show');
  },
  'click #liveGraph': function (e,t) {
    Session.set('graphEdit', 'new');
    Session.set('graphXAxis', []);
    Session.set('graphYAxis', []);
    Session.set('curFunc', 'live');
    resetGraphModal();

    $('#graph-config').modal('show');
  },
  'click #relGraph': function (e,t) {
    Session.set('graphEdit', 'new');
    Session.set('graphXAxis', []);
    Session.set('graphYAxis', []);
    Session.set('curFunc', 'related');
    resetGraphModal();

    $('#graph-config').modal('show');
  },
  'click #setFilters': function (e,t) {
    setFilterModal();
  },
  'click #newMatch': function (e,t) {
    Session.set('newStats', []);

    $('#addMatch').modal('show');
  },
});

Template.Mod.helpers({
  getFaIcon: function () {
    if (this.func === 'live') return 'fa-line-chart';
    if (this.type === 'Values') return 'fa-pie-chart';
    return 'fa-bar-chart';
  },
  selGraph: function () {
    var id = Session.get('graphEdit');
    if (id === false) return {};

    var aux = _.findWhere(this.graphs, {id: id});
    if (!aux) return {};

    return aux;
  },
  getQuery: function () {
    return Session.get('query')
  },
  getLive: function () {
    return Session.get('liveMod')
  },
  getAll: function () {
    return Session.get('allMod')
  },
  showOption: function () {
    return _.contains(arguments, this.func);
  },
  isPublic: function () {
    if (this.public) return '<span class="public-mod">Public<span>';
    return '<span class="private-mod">Private<span>';
  },
  isOwner: function () {
    return this.userId === Meteor.userId();
  },
  isOwnerGraph: function () {
    return Template.parentData(1).userId === Meteor.userId();
  },
});

var graphFunc = {
  count: countPlot,
  live: livePlot,
  related: relPlot,
};

Template.Mod.created = function () {
  Session.set('curFunc', 'count');
  Session.set('graphEdit', false);
  Session.set('liveMod', {limit: 10, sort: {date: -1}});
  Session.set('allMod', {limit: 200, sort: {date: -1}});
  Session.set('fields', []);
  Session.set('graphXAxis', []);
  Session.set('graphYAxis', []);

  if (this.data)
    Session.set('query', {modId: this.data._id, $and: [{'tags.debug': {$ne: 1}}]});

  var self = this;
  this.autorun(function () {
    self.subscribe("modMatches", Session.get('query'), Session.get('liveMod'), function () {
      // Set available fields
      var aux = Matches.findOne();
      if (aux)
        Session.set('fields', Object.keys(aux.stats));
    });
  });

  // Update Live Graphs
  this.autorun(function () {
    if (!self.data) return;
    var mod = Mods.findOne(self.data._id);
    var liveGraphs = _.where(mod.graphs, {func: 'live'});
    var data = Matches.find(Session.get('query'), Session.get('liveMod')).fetch();

    $('#loadingModal').modal('show');

    Meteor.setTimeout(function() {
      liveGraphs.forEach(function (value) {
        var plot = graphFunc[value.func](data, value);

        plot.options.legend = {container: $('#legend'+value.id)};

        if (plot.data.length === 0) {
          $("#legend"+value.id).empty();
          return $("#graph"+value.id).empty();
        }

        if (plot.count > 5)
          $('#'+value.id).removeClass('col-md-6').addClass('col-md-12');
        else
          $('#'+value.id).removeClass('col-md-12').addClass('col-md-6');


        $.plot($("#graph"+value.id), plot.data, plot.options);
      });

      $('#loadingModal').modal('hide');
    }, 200);
  });

  // Update All Graphs
  this.autorun(function () {
    $('#loadingModal').modal('show');

    Meteor.call('getPlots', Session.get('query'), Session.get('allMod'), function (error, result) {
      if (error) return console.log(error);

      result.forEach(function (value) {
        if (value.plot.data.length === 0) {
          $("#legend"+value.id).empty();
          return $("#graph"+value.id).empty();
        }

        if (value.plot.count > 5)
          $('#'+value.id).removeClass('col-md-6').addClass('col-md-12');
        else
          $('#'+value.id).removeClass('col-md-12').addClass('col-md-6');


        value.plot.options.legend = {container: $('#legend'+value.id)};
        $.plot($("#graph"+value.id), value.plot.data, value.plot.options);
      });

      $('#loadingModal').modal('hide');
    });
  });
};

Template.Mod.rendered = function () {
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
};

Template.Mod.destroyed = function () {
  delete Session.keys['curFunc'];
  delete Session.keys['graphs'];
  delete Session.keys['graphEdit'];
  delete Session.keys['query'];
  delete Session.keys['liveMod'];
  delete Session.keys['allMod'];
  delete Session.keys['fields'];
  delete Session.keys['graphXAxis'];
  delete Session.keys['graphYAxis'];
};

ReloadGraph = function (id) {
  $('#loadingModal').modal('show');

  Meteor.call(
    'getPlot',
    id,
    Session.get('query'),
    Session.get('allMod'),
    function (error, result) {
      if (result.plot.data.length === 0) {
        $("#legend"+result.id).empty();
        return $("#graph"+result.id).empty();
      }

      if (result.plot.count > 5)
        $('#'+result.id).removeClass('col-md-6').addClass('col-md-12');
      else
        $('#'+result.id).removeClass('col-md-12').addClass('col-md-6');


      result.plot.options.legend = {container: $('#legend'+result.id)};
      $.plot($("#graph"+result.id), result.plot.data, result.plot.options);
      $('#loadingModal').modal('hide');
    }
  );
}