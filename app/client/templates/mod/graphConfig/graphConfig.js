Template.GraphConfig.events({
  'click .selAxis': function (e,t) {
    var aux = Session.get(e.currentTarget.dataset.array);
    if (!aux) return;

    var key = e.currentTarget.textContent;
    for (var i = 0; i < aux.length; i++) {
      if (aux[i] === key) {
        aux.splice(i, 1);
        return Session.set(e.currentTarget.dataset.array, aux);
      }
    }
  },
  'keydown #xaxis': function (e,t) {
    if (e.keyCode === 13) {
      addXAxis(e,t);
      return false;
    }
  },
  'click #addXAxis': function (e,t) {
    addXAxis(e,t);
  },
  'keydown #yaxis': function (e,t) {
    if (e.keyCode === 13) {
      addYAxis(e,t);
      return false;
    }
  },
  'click #addYAxis': function (e,t) {
    addYAxis(e,t);
  },
  'click #advancedGraph': function (e,t) {
    $('#advancedGraphBox').toggle();
  },
  'click #submitGraph': function (e,t) {
    submitGraph(e,t);
  },
  'submit #graph-config-form': function (e,t) {
    e.preventDefault();
    submitGraph(e,t);
  }
});

Template.GraphConfig.helpers({
  availableFields: function () {
    return Session.get('fields');
  },
  getXAxis: function () {
    return Session.get('graphXAxis');
  },
  getYAxis: function () {
    return Session.get('graphYAxis');
  },
  isSelected: function (cur, type) {
    return (cur === type) ? 'selected' : '';
  },
  showOption: function () {
    return _.contains(arguments, Session.get('curFunc'));
  },
});

resetGraphModal = function () {
  $('#xaxis').val('');
  $('#yaxis').val('');
  $('#graphName').val('');
}

var addXAxis = function (e,t) {
  var axis = Session.get('graphXAxis');
  var aux = t.$('#xaxis').val();
  if (aux !== '' && _.indexOf(axis, aux) === -1)
    axis.push(aux);

  t.$('#xaxis').val('').focus();
  Session.set('graphXAxis', axis);
}

var addYAxis = function (e,t) {
  var axis = Session.get('graphYAxis');
  var aux = t.$('#yaxis').val();
  if (aux !== '' && _.indexOf(axis, aux) === -1)
    axis.push(aux);

  t.$('#yaxis').val('').focus();
  Session.set('graphYAxis', axis);
}

var submitGraph = function (e,t) {
  var mod = Template.parentData(1);
  var id = Session.get('graphEdit');

  var index;
  if (id === 'new')
    index = mod.graphs.length; 
  else {
    for (var i = 0; i < mod.graphs.length; i++) {
      if (mod.graphs[i].id === id) {
        index = i;
        break;
      }
    }
  }

  if (index === -1) return $('#graph-config').modal('hide');

  var aux = {
    name: t.$('#graphName').val(),
    xaxis: Session.get('graphXAxis'),
    yaxis: Session.get('graphYAxis'),
    type: t.$('#graphType').val(),
    func: Session.get('curFunc'),
    id: (id !== 'new') ? id : Random.id(),
  }

  if (!aux.xaxis || aux.xaxis.length === 0) {
    if (t.$('#xaxis').val() !== '')
      aux.xaxis = [t.$('#xaxis').val()];
    else
      delete aux.xaxis;
  }

  if (!aux.yaxis || aux.yaxis.length === 0) {
    if (t.$('#yaxis').val() !== '')
      aux.yaxis = [t.$('#yaxis').val()];
    else
      delete aux.xaxis;
  }

  if (!aux.yaxis && !aux.xaxis)
    return console.log('Missing Axis');

  if (aux.name === '') {
    aux.name = aux.type;
    if (aux.func === 'live') aux.name = "Live Graph";
  }

  if (!aux.type || aux.type === '')
    delete aux.type;

  // Update user prefered graphs
  var str = "graphs." + index;
  var set = {};
  set[str] = aux;
  Mods.update({_id: Template.parentData(1)._id}, {$set: set}, function (e, r) {
    if (e) return console.log(e);

    if (aux.func !== 'live') {
      Meteor.setTimeout(function() {
        ReloadGraph(aux.id);
      }, 100);
    }
  });

  Session.set('graphEdit', false);

  $('#graph-config').modal('hide');
}