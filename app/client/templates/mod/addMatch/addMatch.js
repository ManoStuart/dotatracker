Template.AddMatch.events({
  'click .selAxis': function (e,t) {
    var aux = Session.get('newStats');
    if (!aux) return;

    var key = e.currentTarget.dataset.key;
    for (var i = 0; i < aux.length; i++) {
      if (aux[i].key === key) {
        aux.splice(i, 1);
        return Session.set('newStats', aux);
      }
    }
  },
  'keydown .arrayInput': function (e,t) {
    if (e.keyCode === 13) {
      addStats(e,t);
      return false;
    }
  },
  'click #addStats': function (e,t) {
    addStats(e,t);
  },
  'click #submitMatch': function (e,t) {
    submitMatch(e,t);
  },
  'submit #addMatch-form': function (e,t) {
    e.preventDefault();
    submitMatch(e,t);
  },
});

Template.AddMatch.helpers({
  getStats: function () {
    return Session.get('newStats');
  },
});

var addStats = function (e,t) {
  var axis = Session.get('newStats');
  var aux = {
    key: t.$('#key').val(),
    value: getValue(t.$('#value').val()),
  }

  if (aux.key !== '' && !_.findWhere(axis, {key: aux.key}))
    axis.push(aux);

  t.$('#key').val('').focus();
  t.$('#value').val('');
  Session.set('newStats', axis);
}

var submitMatch = function (e,t) {
  var data = {
    modId: t.data._id,
    stats: {},
    tags: {
      debug: 1,
    },
  }

  Session.get('newStats').forEach(function (value) {
    data.stats[value.key] = value.value;
  });

  Matches.insert(data);

  $('#addMatch').modal('hide');
}

Template.AddMatch.created = function () {
  Session.set('newStats', []);
}

Template.AddMatch.destroyed = function () {
  delete Session.keys['newStats'];
}