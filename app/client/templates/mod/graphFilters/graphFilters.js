Template.GraphFilters.events({
  'click .selAxis': function (e,t) {
    var aux = Session.get(e.currentTarget.dataset.array);
    if (!aux) return;

    var key = e.currentTarget.dataset.key;
    for (var i = 0; i < aux.length; i++) {
      if (aux[i].key === key) {
        aux.splice(i, 1);
        return Session.set(e.currentTarget.dataset.array, aux);
      }
    }
  },
  'keydown .allow': function (e,t) {
    if (e.keyCode === 13) {
      addAllowed(e,t);
      return false;
    }
  },
  'click #addAllow': function (e,t) {
    addAllowed(e,t);
  },
  'keydown .invalid': function (e,t) {
    if (e.keyCode === 13) {
      addInvalid(e,t);
      return false;
    }
  },
  'click #addInvalid': function (e,t) {
    addInvalid(e,t);
  },
  'click #submitFilters': function (e,t) {
    submitFilters(e,t);
  },
  'submit #graphFilters-form': function (e,t) {
    e.preventDefault();
    submitFilters(e,t);
  },
});

Template.GraphFilters.helpers({
  getAllowed: function () {
    return Session.get('allowedTags');
  },
  getInvalid: function () {
    return Session.get('invalidTags');
  },
  getOrder: function () {
    var aux = Session.get('liveMod');

    return Object.keys(aux.sort)[0];
  },
  getDirection: function (type) {
    var aux = Session.get('liveMod');
    var dir = aux.sort[Object.keys(aux.sort)[0]];

    if (dir === 1 && type === 'ascending') return 'selected';
    if (dir === -1 && type === 'descending') return 'selected';

    return '';
  },
});

setFilterModal = function () {
  var query = Session.get('query');
  var allowed = [];
  var denied = [];


  if (query.$and) {
    query.$and.forEach(function (value) {
      var key = Object.keys(value)[0];
      
      if (key.indexOf('tags') !== -1) {
        if (value[key].$ne !== undefined)
          denied.push({key: key.substring(5), value: value[key].$ne});
        else
          allowed.push({key: key.substring(5), value: value[key]});
      }
    });
  }

  Session.set('allowedTags', allowed);
  Session.set('invalidTags', denied);

  $('#graphFilters').modal('show');
}

var addAllowed = function (e,t) {
  var axis = Session.get('allowedTags');
  var aux = {
    key: t.$('#allowkey').val(),
    value: getValue(t.$('#allowvalue').val()),
  }
  if (aux.key !== '')
    axis.push(aux);

  t.$('#allowkey').val('').focus();
  t.$('#allowvalue').val('');
  Session.set('allowedTags', axis);
}

var addInvalid = function (e,t) {
  var axis = Session.get('invalidTags');
  var aux = {
    key: t.$('#invalidkey').val(),
    value: getValue(t.$('#invalidvalue').val()),
  }
  if (aux.key !== '')
    axis.push(aux);

  t.$('#invalidkey').val('').focus();
  t.$('#invalidvalue').val('');
  Session.set('invalidTags', axis);
}

var submitFilters = function (e,t) {
  // Set up query
  var query = {
    modId: Template.parentData(1)._id,
    $and: [],
  }

  if (t.$('#startFilter').val() !== '') {
    query.date = {};
    query.date.$gte = Number(moment(t.$('#startFilter').val(), 'DD/MM/YYYY HH:mm').format('x'));
  }
  if (t.$('#endFilter').val() !== '') {
    if (!query.date) query.date = {};
    query.date.$lt = Number(moment(t.$('#startFilter').val(), 'DD/MM/YYYY HH:mm').format('x'));
  }

  // Add allowed values
  var array = Session.get('allowedTags');
  array.forEach(function (value) {
    var str = 'tags.' + value.key;
    var aux = {};
    aux[str] = value.value;
    query.$and.push(aux);
  });

  // Add denied values
  var array = Session.get('invalidTags');
  array.forEach(function (value) {
    var str = 'tags.' + value.key;
    var aux = {};
    aux[str] = {$ne: value.value};
    query.$and.push(aux);
  });

  if (query.$and.length === 0) delete query.$and;

  Session.set('query', query);

  // Set Modifiers
  var liveMod = {
    limit: Number(t.$('#liveLimit').val()),
  }
  var allMod = {
    limit: Number(t.$('#allLimit').val()),
  }

  var order = t.$('#orderName').val();
  var sort = {date: -1};
  if (order !== '') {
    sort = {};
    sort[order] = (t.$('#orderType').val() === 'ascending') ? 1 : -1;
  }
  liveMod.sort = sort;
  allMod.sort = sort;

  Session.set('liveMod', liveMod);
  Session.set('allMod', allMod);

  $('#graphFilters').modal('hide');
}

Template.GraphFilters.created = function () {
  Session.set('allowedTags', []);
  Session.set('invalidTags', []);
}

Template.GraphFilters.rendered = function () {
  $('.date-picker').datetimepicker({format: "DD/MM/YYYY HH:mm"});
}

Template.GraphFilters.destroyed = function () {
  delete Session.keys['allowedTags'];
  delete Session.keys['invalidTags'];
}