_.extend(App, {
});

App.helpers = {
  isSelected: function (cur, type) {
    return (cur === type) ? 'selected' : '';
  },
  hasDocuments: function (cursor) {
    if (!cursor) return false;

    if (cursor.count)
      return (cursor.count() > 0);

    return (cursor.length > 0);
  },
  currencyValue: function (value) {
    if (value != undefined) return parseFloat(value).toFixed(2);
  },
  getSession: function (name) {
    return Session.get(name);
  },
  
  docsModId: function () {
    return "<span class='docsModId'>modId</span>";
  },
  docsKey: function () {
    return "<span class='docsKey'>key</span>";
  },
  docsParams: function () {
    return "<span class='docsParams'>params</span>";
  },

  myMods: function () {
    return Mods.find({userId: Meteor.userId()});
  }
};

_.each(App.helpers, function (helper, key) {
  Handlebars.registerHelper(key, helper);
});