Template.Api.helpers({
  hasMods: function () {
    return (Mods.find({userId: Meteor.userId()}).count() > 0);
  },
});

Template.Api.rendered = function () {
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
}