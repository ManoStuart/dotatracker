Template.GetStarted.events({
  'click #docsAccount': function (e,t) {
    Session.set('logOpt', true);
    $('#navLogin').click();
    return false;
  },
});

Template.GetStarted.helpers({
  hasAccount: function () {
    return (Meteor.userId()) ? "fa-check-square-o" : "fa-square-o";
  },
  hasMods: function () {
    return (Mods.find({userId: Meteor.userId()}).count() > 0) ? "fa-check-square-o" : "fa-square-o";
  },
})

Template.GetStarted.rendered = function() {
  this.autorun(function () {
    Meteor.call('hasMatches', Meteor.userId(), function (e,r) {
      if (e || !r)
        document.getElementById("docsApiDone").className = "fa fa-square-o";
      else
        document.getElementById("docsApiDone").className = "fa fa-check-square-o";
    });

    Meteor.call('hasGraphs', Meteor.userId(), function (e,r) {
      if (e || !r)
        document.getElementById("docsGraphsDone").className = "fa fa-square-o";
      else
        document.getElementById("docsGraphsDone").className = "fa fa-check-square-o";
    });
  });
};