Template.Docs.events({
  'click #scrollTop': function (e,t) {
    window.scrollTo(0,0);
  },
  'click .question': function (e,t) {
    $('#emailModal').modal('show');
  },
});

Template.Docs.helpers({
  getTemplate: function () {
    return Router.current().params.id;
  },
  isActive: function (item) {
    return (Router.current().params.id === item) ? "active" : "";
  },
  getPage: function () {
    return pageNames[Router.current().params.id];
  },
});

var pageNames = {
  GetStarted: "Getting Started",
  Modeling: "Modeling",
  Api: "Api",
  LiveGraphs: "Live Graphs",
  FullGraphs: "Full Graphs",
  RelationGraphs: "Relation Graphs",
  Filters: "Filters",
  Examples: "Examples",
}