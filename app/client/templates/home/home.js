Template.Home.events({
  'click .collapse': function (e,t) {
    $(e.currentTarget).toggleClass('on');
    $(e.currentTarget).parents('.portlet').find('.portlet-body').slideToggle(200);
  },
});


Template.Home.helpers({
  getDate: function (str) {
    return moment(str, "DD/MM/YYYY").format('DD/MM/YYYY');
  },
  publicMods: function () {
    return Mods.find({public: true});
  }
});


Template.Home.rendered = function () {
  // Update Live Graphs
  this.autorun(function () {
    var data = Mods.find({}, {sort: {matchCount: -1}});

    var len = data.count();

    if (data.count() === 0) {
      $("#legend").empty();
      return $("#graph").empty();
    }
    len = (len < 15) ? 15 : len - 0.5;

    var plot = {
      data: [],
      options: {
        xaxis: {
          tickLength: 0,
          ticks: [],
          min: -0.505,
          max: len
        },
        series: {
          bars: {
            show: true,
            barWidth: 0.9,
            align: 'center',
          },
        },
        grid: {
          borderWidth: {
            top: 0, right: 0, left: 2, bottom: 2
          },
          hoverable: true,
          clickable: true,
        },
        tooltip: {
          show: true,
          content: function(label, xval, yval, flotItem) {
            var mod = Mods.findOne({slug: label}, {fields: {name: 1}});
            return yval + ' - ' +((mod) ? mod.name : label);
          },
        },
        legend: {
          container: $('#legend'),
          labelFormatter: function(label, series) {
            var mod = Mods.findOne({slug: label}, {fields: {name: 1}});
            return '<a href="/mod/' + label + '">' + ((mod) ? mod.name : label) + '</a>';
          }
        },
      },
      count: 0,
    }

    var aux = [];
    data.forEach(function (value, index) {
      plot.data.push({
        label: value.slug,
        data: [[index, value.matchCount]]
      });
    });

    $.plot($("#graph"), plot.data, plot.options);
  });

  $("#graph").bind("plotclick", function (event, pos, item) {
    if (item) {
      $('.flotTip').remove();
      Router.go('Mod', {slug: item.series.label});
    }
  });
};

Template.Home.destroyed = function () {
};
