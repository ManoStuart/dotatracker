OpenDeleteModal = function (id, classStr) {
  var button = $('.deleteModalSubmit')[0];

  if (!button) return;

  button.className = 'btn blue deleteModalSubmit ' + classStr;
  button.dataset.id = id;

  $('#deleteModal').modal('show');
}