OpenImageModal = function (imageId, postId) {
  Session.set('imageModal', postId);

  Meteor.setTimeout(function() {
    $("#carouselModal").owlCarousel({
      items : 1,
      slideSpeed : 400,
      navigation: true,
      navigationText: ['',''],
      pagination: false,
      paginationSpeed : 1000,
      goToFirstSpeed : 2000,
      transitionStyle:"fade",
      autoHeight : true,
    });

    var owl = $("#carouselModal").data('owlCarousel');
     
    var post = Posts.findOne(postId);
    var images = _.where(post.images, {gallery: false});

    var index;
    images.forEach(function (value, i){
      if (value.id == imageId)
        return index = i;
    });

    $('#imageModal').modal('show');

    if (index !== undefined)
    Meteor.setTimeout(function() {owl.jumpTo(index);}, 400);
  }, 100);
}

Template.ImageModal.events({
  'mouseover .imageModalBox': function (e, t) {
    $(e.currentTarget).find('button').show();
  },
  'mouseout .imageModalBox': function (e, t) {
    $(e.currentTarget).find('button').hide();
  },
  'click .imageModalLeft': function (e,t) {
    var postId = $('#imageModal-img').data('post');
    var imageId = $('#imageModal-img').data('image');

    var post = Posts.findOne(postId);
    var images = _.where(post.images, {gallery: false});

    var index;
    images.forEach(function (value, i){
      if (value.id == imageId)
        return index = i;
    });

    if (index === undefined) return;

    index--;
    index = (index < 0) ? images.length - 1 : index;
    
    $('#imageModal-img')
      .attr("src", 'https://s3-us-west-2.amazonaws.com/casa-de-valentina/' + images[index].url)
      .data('image', images[index].id);

  },
  'click .imageModalRight': function (e,t) {
    var postId = $('#imageModal-img').data('post');
    var imageId = $('#imageModal-img').data('image');

    var post = Posts.findOne(postId);
    var images = _.where(post.images, {gallery: false});

    var index;
    images.forEach(function (value, i){
      if (value.id == imageId)
        return index = i;
    });

    if (index === undefined) return;

    index++;
    index = (index >= images.length) ? 0 : index;
    
    $('#imageModal-img')
      .attr("src", 'https://s3-us-west-2.amazonaws.com/casa-de-valentina/' + images[index].url)
      .data('image', images[index].id);

  },
});

Template.ImageModal.helpers({
  imagesModal: function () {
    var post = Posts.findOne(Session.get('imageModal'));
    if (post)
      return _.where(post.images, {gallery: false});
  }
});


Template.ImageModal.created = function () {
  Session.set('imageModal', false);
}