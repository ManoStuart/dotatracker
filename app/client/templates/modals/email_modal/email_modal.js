Template.EmailModal.events({
  'submit #emailForm': function (e,t) {
    sendEmailModal(e,t);
  },
  'click #submitEmail': function (e,t) {
    sendEmailModal(e,t);
  },
});

var sendEmailModal = function (e,t) {
  e.preventDefault();

  if (t.$('#content').val() === '') {
    t.$('#content-error').text("This field is required.");
    t.$('#content').focus();

    Meteor.setTimeout(function() {t.$('#content-error').text('');}, 2000);
    return;
  }

  Meteor.call('sendMail',
    t.$('#subject').val(),
    t.$('#content').val(),
    t.$('#from').val(),
    function (error, result) {
      t.$('#emailModal').modal('hide');
      if (error) return OpenInfoModal(error.toString(), 'alert alert-danger');

      OpenInfoModal("Email sent!", 'alert alert-success');
    }
  );
}