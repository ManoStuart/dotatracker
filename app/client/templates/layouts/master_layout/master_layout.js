Template.MasterLayout.onCreated(function () {
  this.subscribe('myMods');
});

Template.Footer.events({
  'click #contact': function (e,t) {
    $('#emailModal').modal('show');
  },
});

Template.LoginMenu.events({
  'click .changeOpt': function (e,t) {
    Session.set('logOpt', !Session.get('logOpt'));

    // Prevent shit from closing
    e.stopPropagation();
    return false;
  },

  'submit #loginForm': function (e, t) {
    e.preventDefault();

    var username = t.$('#username').val();
    var password = t.$('#passwordLogin').val();

    if (Session.get('logOpt')) {
      var confirm = t.$('#confirmPassword').val();
      if (password !== confirm) return OpenInfoModal('Passwords differ!', 'alert alert-danger');

      Accounts.createUser({username: username, password: password}, function (error, result) {
        if (error) return OpenInfoModal(error.toString(), 'alert alert-danger');
      });
    } else {
      Meteor.loginWithPassword(username, password, function (error) {
        if (error) return OpenInfoModal(error.toString());
      });
    }

    Session.set('logOpt', false);
  },

  'click #btnLogout': function (e,t) {
    Meteor.logout(function (error) {
      if(error) return OpenInfoModal(error.toString(), 'alert alert-danger');
    });
  },
});

Template.LoginMenu.helpers({
  getPasswordOpt: function () {
    if (Session.get('logOpt')) return 'Cancel';

    return 'Change Password';
  },
  getSubmit: function () {
    if (Session.get('logOpt')) return 'CREATE USER';

    return 'LOG IN';
  },
  getChangeOpt: function () {
    if (Session.get('logOpt')) return 'Log In';

    return 'Sign Up';
  },
});

Template.LoginMenu.created = function () {
  Session.set('logOpt', false);
}

Template.LoginMenu.destroyed = function () {
  delete Session.keys['logOpt'];
}