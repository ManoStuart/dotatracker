Template.NewMod.events({
  'submit #modForm': function(e, t) {
    e.preventDefault();

    if (!Meteor.userId())
      return OpenInfoModal("No user logged in!", 'alert alert-danger');

    var data = {
      "name": t.$("#name").val(),
      "url": t.$("#url").val(),
      "description": t.$('#description').val(),
    }

    $('#loadingModal').modal('show');
    if (Router.current().route.getName() === 'EditMod') {
      Mods.update({_id: t.data._id}, {$set: data}, function (e,r) {
        $('#loadingModal').modal('hide');
        if (e) return OpenInfoModal(e.toString(), 'alert alert-danger');

        OpenInfoModal ("Updated mod info.", 'alert alert-success');
      });
    } else {
      Mods.insert(data, {validate: false}, function (e,r) {
        $('#loadingModal').modal('hide');
        if (e) return OpenInfoModal(e.toString(), 'alert alert-danger');

        Router.go('EditMod', {id: r});
      });
    }
  },
});

Template.NewMod.helpers({
  HasMod: function () {
    return (Router.current().route.getName() === 'EditMod' && this._id);
  },
  getType: function () {
    return (Router.current().route.getName() === 'EditMod') ? 'Edit' : 'New';
  },
});

Template.NewMod.created = function () {
};

Template.NewMod.rendered = function () {
};

Template.NewMod.destroyed = function () {
};