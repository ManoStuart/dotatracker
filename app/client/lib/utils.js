addIndex = function (array) {
  return _.map(array, function (value, index) {
    value.index = index;
    return value;
  });
}