var newInfoObj = function (array) {
  return {
    Sum: 0,
    Max: 0,
    Min: 9007199254740991, // Number.MAX_SAFE_INTEGER
    Values: {},
    Exists: 0,
    Average: 0,
    ZoneAverage: 0,
    Len: 0, // total array length
  };
}

var isComputable = function (value) {
  return (typeof value === "string" || typeof value === "number");
}

function fetchFromObject(obj, prop) {
  if(typeof obj === 'undefined') {
    return false;
  }

  var _index = prop.indexOf('.')
  if(_index > -1) {
    return fetchFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
  }

  return obj[prop];
}

var parseIteration = function (obj, value) {
  if (value)
    obj.Exists += 1;

  if (!isNaN(value)) {
    obj.Sum += value;
    if (obj.Max < value) obj.Max = value;
    if (obj.Min > value) obj.Min = value;
  }

  if (!obj.Values[value])
    obj.Values[value] = 1;
  else
    obj.Values[value] += 1;

  obj.Len ++;

  return obj;
}

var countParse = function (array, field) {
  var aux = newInfoObj(array);

  if (!array, array.length === 0) return aux;

  array.forEach(function (value) {
    //  Get analysed field
    // var tmp = value.stats[field];
    var tmp = fetchFromObject(value.stats, field);

    if (Object.prototype.toString.call(tmp) === '[object Array]') {
      tmp.forEach(function (elem) {
        if (isComputable(elem))
          aux = parseIteration(aux, elem);
      });
    } else {
      if (isComputable(tmp))
        aux = parseIteration(aux, tmp);
    }
  });

  aux.Average = aux.Sum / aux.Len;

  return aux;
}

countPlot = function (data, options) {
  if (!options || !options.xaxis) return;

  var plot = {
    data: [],
    options: {
      xaxis: {
        tickLength: 0,
        ticks: [],
        min: -0.6,
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.8,
          align: 'center',
        },
      },
      grid: {
        borderWidth: {
          top: 0, right: 0, left: 2, bottom: 2
        },
        hoverable: true,
      },
      tooltip: {
        show: true,
        content: "%y",
      },
    },
    count: 0,
  }
  if (options.type === 'Values') {
    delete plot.options.xaxis;
    delete plot.options.tooltip;

    plot.options.series = {
      pie: {
        show: true,
        radius: 1,
        label: {
            show: true,
            radius: 2/3,
            formatter: function (name, data) {return parseFloat(data.percent).toFixed(2) + '%';},
            threshold: 0.1,
        },
      }
    };
  }

  var len = 0;

  options.xaxis.forEach(function (value, index) {
    var parsed = countParse(data, value);

    if (len < parsed.Len) len = parsed.Len;

    var tmp;
    if (options.type === 'Overview') {
      plot.data.push({
        label: value,
        data: [[0, parsed.Average], [1, parsed.Max], [2, parsed.Min]]
      });
      plot.options.xaxis.ticks = [[0, 'Average'], [1, 'Max'], [2, 'Min']];
      plot.count = 3;
    } else if (options.type === 'Values') {
      for (var key in parsed.Values) {
        plot.data.push({
          label: key,
          data: parsed.Values[key],
        });
      }
    } else {
      plot.data.push({
        label: value,
        data: [[index, parsed[options.type]]],
      });
      plot.options.xaxis.ticks.push([index, value]);
      plot.count ++;
    } 
  });

  if (options.type === 'Exists')
    plot.options.yaxis = {max: len};

  return plot;
}

var liveIteration = function (plot, value, date, key, conversion) {
  if (typeof value === 'string') {
    if (!conversion[key]) conversion[key] = {$count: 1};

    if (!conversion[key][value]) { 
      conversion[key][value] = conversion[key].$count;
      value = conversion[key][value];
      conversion[key].$count ++;
    } else {
      value = conversion[key][value];
    }
  }

  plot.push([date, value]);

  // return conversion;
}

livePlot = function (data, options) {
  if (!options || !options.yaxis) return;

  var conversion = {};

  var plot = {
    data: [],
    options: {
      xaxis: {
        mode: 'time',
      },
      series: {
        // lines: {show: true},
        points: {show: true},
      },
      grid: {
        borderWidth: {
          top: 0, right: 0, left: 2, bottom: 2
        },
        hoverable: true
      },
      tooltip: {
        show: true,
        content: function(label, xval, yval, flotItem) {
          if (conversion[label]) {
            for (var key in conversion[label]) {
              if (conversion[label][key] === yval) return key;
            }
          }

          return yval+'';
        },
      },
    },
    count: data.length,
  }

  // initiate data fields
  plot.data = _.map(options.yaxis, function (key) {return {label: key, data: []}});

  data.forEach(function (value) {
    options.yaxis.forEach(function (key, index) {
      var tmp = value.stats[key];
      var tmp = fetchFromObject(value.stats, key);

      if (Object.prototype.toString.call(tmp) === '[object Array]') {
        tmp.forEach(function (elem) {
          if (isComputable(elem))
            liveIteration(plot.data[index].data, elem, value.date, key, conversion);
        });
      } else {
        if (isComputable(tmp))
          liveIteration(plot.data[index].data, tmp, value.date, key, conversion);
      }
    });
  });

  return plot;
}

var relParse = function (array, field, baseField) {
  var aux = {$len: 0}

  if (!array, array.length === 0) return aux;

  array.forEach(function (value) {
    //  Get analysed fields
    // var tmp = value.stats[field];
    var tmp = fetchFromObject(value.stats, field);
    // var base = value.stats[baseField];
    var base = fetchFromObject(value.stats, baseField);

    if (!aux[base]) aux[base] = newInfoObj(array);

    if (Object.prototype.toString.call(tmp) === '[object Array]') {
      tmp.forEach(function (elem) {
        if (isComputable(elem)) {
          aux.$len ++;
          aux[base] = parseIteration(aux[base], elem);
        }
      });
    } else {
      if (isComputable(tmp)) {
        aux.$len ++;
        aux[base] = parseIteration(aux[base], tmp);
      }
    }
  });

  for (var key in aux) {
    if (key === '$len') continue;

    aux[key].Average = aux[key].Sum / aux.$len;
    aux[key].ZoneAverage = aux[key].Sum / aux[key].Len;
  }

  return aux;
}

relPlot = function (data, options) {
  if (!options || !options.xaxis || !options.yaxis) return;

  var plot = {
    data: [],
    options: {
      xaxis: {
        tickLength: 0,
        ticks: [],
        min: -0.6,
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.8,
          align: 'center',
        },
      },
      grid: {
        borderWidth: {
          top: 0, right: 0, left: 2, bottom: 2
        },
        hoverable: true
      },
      tooltip: {
        show: true,
        content: "%y"
      },
    },
    count: 0,
  }
  if (options.type === 'Values') {
    delete plot.options.xaxis;
    delete plot.options.tooltip;

    plot.options.series = {
      pie: {
        show: true,
        radius: 1,
        label: {
            show: true,
            radius: 2/3,
            formatter: function (name, data) {return parseFloat(data.percent).toFixed(2) + '%';},
            threshold: 0.1
        }
      }
    };
  }

  var baseField = options.xaxis[0];
  var len = 0;

  options.yaxis.forEach(function (value, index) {
    var parsed = relParse(data, value, baseField);
    
    if (len < parsed.$len) len = parsed.$len;

    var i = 0;
    for (var key in parsed) {
      if (key === '$len') continue;

      if (options.type === 'Overview') {
        plot.data.push({
          label: value + ' : ' + key, 
          data: [[i*3, parsed[key].ZoneAverage], [(i*3)+1, parsed[key].Max], [(i*3)+2, parsed[key].Min]]
        });
        plot.options.xaxis.ticks = plot.options.xaxis.ticks.concat(
          [[i*3, 'Zone Average'], [(i*3)+1, 'Max'], [(i*3)+2, 'Min']]
        );
        if (plot.count < i * 3) plot.count = i*3;
      } else if (options.type === 'Values') {
        for (var key2 in parsed[key].Values) {
          plot.data.push({
            label: key + ' : ' + key2,
            data: parsed[key].Values[key2],
          });
        }
      } else {
        plot.data.push({
          label: value + ' : ' + key,
          data: [[i, parsed[key][options.type]]],
        });
        plot.options.xaxis.ticks.push([i, key]);
        plot.count ++;
      }

      i++;
    }
  });

  if (options.type === 'Exists')
    plot.options.yaxis = {max: len};

  return plot;
}