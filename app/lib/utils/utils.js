getValue = function (value) {
  if (isNaN(value)) return value;

  return Number(value);
}