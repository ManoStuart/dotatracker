Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {name: 'Home', where: 'client'});

Router.route('/mod/new', {name: 'NewMod', where: 'client'});
Router.route('/mod/edit/:id', {name: 'EditMod', where: 'client', controller: 'NewModController', template: 'NewMod'});
Router.route('/mod/:slug', {name: 'Mod', where: 'client'});

Router.route('/docs', function () {this.redirect('/docs/GetStarted');});
Router.route('/docs/:id', {name: 'Docs', where: 'client'});

Router.route('newmatch', {path: '/match', where: 'server'})
  .post(function () {
    console.log("body:");
    console.log(this.request.body);
    this.response.end('post request\n');
  });