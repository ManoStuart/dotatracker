DocsController = RouteController.extend({
  waitOn: function () {
  },

  data: function () {
  },

  action: function () {
    this.render();
    window.scrollTo(0, 0);
  },
});
