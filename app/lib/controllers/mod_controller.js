ModController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("singleMod", this.params.slug),
    ];
  },

  data: function () {
    var aux = Mods.findOne({slug: this.params.slug});
    if (!aux)
      return Session.set('graphs', []);
    else
      Session.set('graphs', aux.graphs);

    return aux;
  },

  action: function () {
    this.render();
  }
});
