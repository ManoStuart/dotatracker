NewModController = RouteController.extend({
  waitOn: function () {
    if (!this.params.id) return;
    return [
      Meteor.subscribe("editMod", this.params.id),
    ];
  },

  data: function () {
    return Mods.findOne(this.params.id);
  },

  action: function () {
    this.render();
  }
});
