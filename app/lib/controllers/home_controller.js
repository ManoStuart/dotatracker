HomeController = RouteController.extend({
  waitOn: function () {
    return [
      Meteor.subscribe("publicMods", 15),
    ];
  },

  data: function () {
  },

  action: function () {
    this.render();
  }
});
