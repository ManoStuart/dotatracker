// Saves = new Mongo.Collection('saves');

SavesSchema =  new SimpleSchema({
  modId: {
    type: String,
  },
  steamId: {
    type: String,
  },

  // Used to store tags to filter the search
  tags: {
    type: Object,
    blackbox: true,
  },

  // Used to store player stats, ie: Lvl, gold ..
  stats: {
    type: Object,
    blackbox: true,
  },
});

// Saves.attachSchema(SavesSchema);