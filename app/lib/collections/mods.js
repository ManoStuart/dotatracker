Mods = new Mongo.Collection('mods');

ModsSchema =  new SimpleSchema({
  key: {
    type: String,
    autoValue: function () {
      if (this.isInsert) {
        return Random.id();
      } else {
        this.unset();
      }
    }
  },

  userId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.userId;
      } else {
        this.unset();
      }
    }
  },

  name: {
    type: String,
  },
  slug: {
    type: String,
    index: true,
    unique: true,
    optional: true,
  },
  description: {
    type: String,
    optional: true,
  },
  public: {
    type: Boolean,
    defaultValue: false,
  },
  url: {
    type: String,
  },

  // Save user prefered graphs
  graphs: {
    type: [Object],
    defaultValue: [],
    optional: true,
  },
  "graphs.$.name": {
    type: String,
  },
  "graphs.$.id": {
    type: String,
  },
  "graphs.$.xaxis": {
    type: [String],
    optional: true,
  },
  "graphs.$.yaxis": {
    type: [String],
    optional: true,
  },
  "graphs.$.type": {
    type: String,
    optional: true,
    allowedValues: ['Average', 'ZoneAverage', 'Max', 'Min', 'Overview', 'Sum', 'Exists', 'Values'],
  },
  "graphs.$.func": {
    type: String,
    allowedValues: ['count', 'related', 'live'],
  },

  // Matches parsed info
  matchCount: {
    type: Number,
    autoValue: function () {
      if (this.isInsert) {
        return 0;
      }
    }
  }
});

Mods.attachSchema(ModsSchema);