UserProfileSchema = new SimpleSchema({
  steamId: {
    type: String,
  },
});

UserSchema = new SimpleSchema({
  username: {
    type: String,
    regEx: /^[a-z0-9A-Z_]{3,15}$/
  },

  emails: {
    type: [Object],
    optional: true
  },
  "emails.$.address": {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  "emails.$.verified": {
    type: Boolean
  },

  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert)
        return new Date();
      else
        this.unset();
    },
  },
  profile: {
    type: UserProfileSchema,
    optional: true
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true
  },

  roles: {
    type: Object,
    optional: true,
    blackbox: true
  },
});

Meteor.users.attachSchema(UserSchema);