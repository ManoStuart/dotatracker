Matches = new Mongo.Collection('matches');

MatchesSchema =  new SimpleSchema({
  modId: {
    type: String,
  },
  // timestamp (not unix)
  date: {
    type: Number,
    autoValue: function () {
      if (this.isInsert)
        return Number(moment(new Date()).format('x'));
      else
        this.unset();
    },
  },

  // Used to store tags to filter the search
  tags: {
    type: Object,
    blackbox: true,
    optional: true,
  },

  // Used to store info regarding the whole game
  stats: {
    type: Object,
    blackbox: true,
    optional: true,
  },

  // Used to store 2way-bound info
  // Still have no ideia of how to do this shit.
  // events: {
  //   type: [Object],
  //   optional: true,
  // },
  // "events.$.key": {
  //   type: Object,
  //   blackbox: true,
  // },
  // "events.$.value": {
  //   type: Object,
  //   blackbox: true,
  // },
});

Matches.attachSchema(MatchesSchema);